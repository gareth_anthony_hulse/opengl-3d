#pragma once

#include "texture.hpp"
#include "vertex.hpp"
#include "shader.hpp"

#include <vector>

namespace app
{
  class mesh
  {
  private:
    // Variables:
    unsigned int m_vao, m_vbo, m_ebo;

    // Funtions:
    void m_setup_mesh ();
    
  public:
    // Variables:
    std::vector<vertex> vertices;
    std::vector<unsigned int> indices;
    std::vector<texture> textures;
    unsigned int shader;
    
    // Functions:
    void draw ();
    void setup ();
  };
}
