#include "../mesh.hpp"

extern "C"
{
#include <GL/glew.h>
#include <GLFW/glfw3.h>
}

#include <string>

namespace app
{
  // Public:
  void
  mesh::setup ()
  {
    glGenVertexArrays (1, &m_vao);
    glGenBuffers (1, &m_vbo);
    glGenBuffers (1, &m_ebo);
    glBindVertexArray (m_vao);
    glBindBuffer (GL_ARRAY_BUFFER, m_vbo);
    glBufferData (GL_ARRAY_BUFFER, vertices.size () * sizeof (vertex), &vertices[0], GL_STATIC_DRAW);
    glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, m_ebo);
    glBufferData (GL_ELEMENT_ARRAY_BUFFER, indices.size () *  sizeof (unsigned int), &indices[0], GL_STATIC_DRAW);

    // Vertex positions:
    glEnableVertexAttribArray (0);
    glVertexAttribPointer (0, 3, GL_FLOAT, GL_FALSE, sizeof (vertex), static_cast<void *> (0));
    // Vertex normals:
    glEnableVertexAttribArray (1);
    glVertexAttribPointer (1, 3, GL_FLOAT, GL_FALSE, sizeof (vertex), reinterpret_cast<void *> (offsetof (vertex, normal)));
    // Vertex texture coordinates:
    glEnableVertexAttribArray (2);
    glVertexAttribPointer (2, 2, GL_FLOAT, GL_FALSE, sizeof (vertex), reinterpret_cast<void *> (offsetof (vertex, tex_coords)));
    // Vetex tangent:
    glEnableVertexAttribArray (3);
    glVertexAttribPointer (3, 3, GL_FLOAT, GL_FALSE, sizeof (vertex), reinterpret_cast<void *> (offsetof (vertex, tangent)));
    // Vertex bitanget:
    glEnableVertexAttribArray (4);
    glVertexAttribPointer (4, 3, GL_FLOAT, GL_FALSE, sizeof (vertex), reinterpret_cast<void *> (offsetof (vertex, bitangent)));

    glBindVertexArray (0);
  }

  void
  mesh::draw ()
  {
    // Bind textures:
    unsigned int
      diffuse_nr = 1,
      specular_nr = 1,
      normal_nr = 1,
      height_nr = 1;
 
    for (unsigned int i = 0; i < textures.size (); ++i)
      {
	glActiveTexture (GL_TEXTURE0 + i);
	std::string
	  number,
	  name = textures[i].type;
	
	if (name == "texture_diffuse")
	  {
	    number = std::to_string(++diffuse_nr);
	  }
	else if (name == "texture_specular")
	  {
	    number = std::to_string(++specular_nr);
	  }
	else if (name == "texture_normal")
	  {
	    number = std::to_string(++normal_nr);
	  }
	else if (name == "texture_height")
	  {
	    number = std::to_string(++height_nr);
	  }

	glUniform1i(glGetUniformLocation(shader, (name + number).c_str()), i);
	glBindTexture(GL_TEXTURE_2D, textures[i].id);
      }
  }
}
