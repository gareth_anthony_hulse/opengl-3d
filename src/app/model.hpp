#pragma once

#include "mesh.hpp"

#include <string>
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <vector>

namespace app
{
  class model
  {
  private:
    // Variables:
    std::vector<mesh> m_meshes;
    std::string m_directory;

    // Functions:
    void m_load (std::string path);
    void m_process_node (aiNode *node, const aiScene *scene);
    mesh m_process_mesh (aiMesh *mesh, const aiScene *scene);
    
  public:
    // Variables:
    unsigned int shader;

    // Functions:
    model (std::string path);
    
  };
};
