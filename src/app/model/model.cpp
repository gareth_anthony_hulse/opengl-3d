#include "../model.hpp"
#include "../shader.hpp"

#include <iostream>

/*
namespace app
{
  // Private:
  void
  model::m_load (std::string path)
  {
    Assimp::Importer import;
    const aiScene *scene = import.ReadFile (path, aiProcess_Triangulate | aiProcess_FlipUVs);
    if (!scene or scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE or !scene->mRootNode)
      {
	std::cerr << "ERROR:ASSIMP::" << import.GetErrorString () << "\n";
	return;
      }

    m_directory = path.substr(0, path.find_last_of('/'));
    m_process_node (scene->mRootNode, scene);
  }

  void
  model::m_process_node (aiNode *node, const aiScene *scene)
  {
    for (unsigned int i = 0; i < node->mNumMeshes; ++i)
      {
	aiMesh *mesh = scene->mMeshes[node->mMeshes[i]];
	m_meshes.push_back (m_process_mesh (mesh, scene));
      }
  }

  mesh
  model::m_process_mesh (aiMesh *local_mesh, const aiScene *scene)
  {
    std::vector<vertex> vertices;
    std::vector<unsigned int> indices;
    std::vector<texture> textures;

    for (unsigned int i = 0; i < local_mesh->mNumVertices; ++i)
      {
	vertex local_vertex;
	glm::vec3 vector;
	vector.x = local_mesh->mVertices[i].x;
	vector.x = local_mesh->mVertices[i].y;
	vector.x = local_mesh->mVertices[i].z;
	local_vertex.position = vector;
	// Normals:
	if (local_mesh->HasNormals ())
	  {
	    vector.x = local_mesh->mNormals[i].x;
	    vector.x = local_mesh->mNormals[i].y;
	    vector.x = local_mesh->mNormals[i].z;
	    local_vertex.normal = vector;
	  }

	if (local_mesh->mTextureCoords[0])
	  {
	    glm::vec2 vec;
	    vec.x = local_mesh->mTextureCoords[0][i].x; 
	    vec.y = local_mesh->mTextureCoords[0][i].y;
	    local_vertex.tex_coords = vec;
	    
	    vector.x = local_mesh->mTangents[i].x;
	    vector.y = local_mesh->mTangents[i].y;
	    vector.z = local_mesh->mTangents[i].z;
	    local_vertex.tangent = vector;
	    
	    vector.x = local_mesh->mBitangents[i].x;
	    vector.y = local_mesh->mBitangents[i].y;
	    vector.z = local_mesh->mBitangents[i].z;
	    local_vertex.bitangent = vector;
	  }
      }

    aiMaterial* material = scene->mMaterials[local_mesh->mMaterialIndex];
    
    return tmp_mesh;
  }
  
  // Public:
  model::model (std::string path)
  {
    m_load (path);
  }
};
*/
