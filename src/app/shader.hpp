#pragma once

extern "C"
{
#include <GL/glew.h>
}
#include <glm/glm.hpp>
#include <string>

namespace app
{
  class shader
  {
  private:
    unsigned int m_vertex_shader, m_fragment_shader;
  public:
    std::string vertex, fragment;

    ~shader ();
    unsigned int compile ();
  };
};
