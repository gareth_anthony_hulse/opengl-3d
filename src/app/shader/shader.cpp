#include "../shader.hpp"

#include <iostream>

namespace app
{
  // Public:
  shader::~shader ()
  {
    glDeleteShader (m_vertex_shader);
    glDeleteShader (m_fragment_shader);
  }
  
  unsigned int
  shader::compile ()
  {
    // Vertex shader:
    m_vertex_shader = glCreateShader(GL_VERTEX_SHADER);
    const char *c_vertex = vertex.c_str ();
    glShaderSource (m_vertex_shader, 1, &c_vertex, nullptr);
    glCompileShader (m_vertex_shader);
    int success;
    glGetShaderiv (m_vertex_shader, GL_COMPILE_STATUS, &success);  
    if (!success)
      {
	char info_log[512];
	glGetShaderInfoLog (m_vertex_shader, sizeof (info_log), nullptr, info_log);
	std::cout << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << info_log << "\n";	  
      }

    // Fragment shader:
    m_fragment_shader = glCreateShader (GL_FRAGMENT_SHADER);
    const char *c_fragment = fragment.c_str ();
    glShaderSource (m_fragment_shader, 1, &c_fragment, nullptr);
    glCompileShader (m_fragment_shader);
    glGetShaderiv (m_fragment_shader, GL_COMPILE_STATUS, &success);
    if (!success)
      {
	char info_log[512];
	glGetShaderInfoLog (m_vertex_shader, sizeof (info_log), nullptr, info_log);
	std::cout << "ERROR::SHADER:FRAGMENT::COMPILATION_FAILED\n" << info_log << "\n";
      }

    unsigned int shader_program = glCreateProgram ();
    glAttachShader (shader_program, m_vertex_shader);
    glAttachShader (shader_program, m_fragment_shader);
    glLinkProgram (shader_program);

    glGetProgramiv (shader_program, GL_LINK_STATUS, &success);
    if (!success)
      {
	char info_log[512];
	glGetProgramInfoLog (shader_program, sizeof (info_log), nullptr, info_log);
      }
    
    return shader_program;
  }
};
