#pragma once

#include <string>

namespace app
{
  struct texture
  {
    unsigned int id;
    std::string type, path;
  };
}
