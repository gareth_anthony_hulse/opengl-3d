#pragma once

#include <glm/glm.hpp>

namespace app
{
  struct vertex
  {
    glm::vec3 position, normal, tangent, bitangent;
    glm::vec2 tex_coords;
  };
}
