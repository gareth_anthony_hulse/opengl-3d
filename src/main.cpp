#define STB_IMAGE_IMPLEMENTATION

#include "app/mesh.hpp"
#include "app/shader.hpp"
#include "app/model.hpp"

extern "C"
{
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <stb/stb_image.h>
}

#include <iostream>
#include <fstream>
#include <any>
#include <cmath>
#include <thread>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <memory>

namespace
{
  GLFWwindow *window;
  /*  float tex_coords[] =
    {
      0.0f, 0.0f,  // lower-left corner  
      1.0f, 0.0f,  // lower-right corner
      0.5f, 1.0f   // top-center corner
      },*/
  /*boarder_colour[] =
    {
      1.0f, 1.0f, 0.0f, 1.0f
      };*/
  
  inline
  void process_input ()
  {
    if (glfwGetKey (window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
      {
	glfwSetWindowShouldClose (window, true);
      }
  }

  inline std::string
  load_file (std::string file)
  {
    std::ifstream in (file);
    std::string  file_content;
    if (in.is_open ())
      {   
	while (!in.eof ())
	  {
	    std::string s;
	    getline (in, s);
	    file_content += s + "\n";
	    
	  }
      }
    in.close ();
    return file_content;
  }
}

int
main ()
{
  // Create GLFW window:
  if (!glfwInit ())
    {
      std::cerr << "GLFW failed to initalise!" << std::endl;
      exit (EXIT_FAILURE);
    }

  GLFWmonitor* monitor = glfwGetPrimaryMonitor();
  const GLFWvidmode* mode = glfwGetVideoMode(monitor);
  glfwWindowHint(GLFW_RED_BITS, mode->redBits);
  glfwWindowHint(GLFW_GREEN_BITS, mode->greenBits);
  glfwWindowHint(GLFW_BLUE_BITS, mode->blueBits);
  glfwWindowHint(GLFW_REFRESH_RATE, mode->refreshRate);

  glfwWindowHint (GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint (GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint (GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
  glfwWindowHint (GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

  window = glfwCreateWindow (mode->width, mode->height, PACKAGE, monitor, NULL);
  if (!window)
    {
      glfwTerminate ();
      std::cerr << "Failed to create window!" << std::endl;
      exit (EXIT_FAILURE);
    }
  
  glfwMakeContextCurrent(window);
  glewInit (); // Needed to avoid segmentation fault.

  glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
  glfwSwapInterval(0);

  // Square:
  glClearColor(0.3f, 0.0f, 0.2f, 1.0f);
  
  float vertices[] =
    {
      // Positions:          // Colors:           // Texture coords:
      0.5f,  0.5f, 0.0f,   1.0f, 0.0f, 0.0f,   1.0f, 1.0f,   // top right
      0.5f, -0.5f, 0.0f,   0.0f, 1.0f, 0.0f,   1.0f, 0.0f,   // bottom right
      -0.5f, -0.5f, 0.0f,  0.0f, 0.0f, 1.0f,   0.0f, 0.0f,   // bottom left
      -0.5f,  0.5f, 0.0f,  1.0f, 1.0f, 0.0f,   0.0f, 1.0f    // top left 
    };

  // load vertex and fragment shader, then compile.
  app::shader my_shader;
  my_shader.vertex = load_file (SHADER_DIR "/shader.vert");
  my_shader.fragment = load_file (SHADER_DIR "/shader.frag");
  auto shader_program = my_shader.compile ();

  unsigned int
    indices[] =
    {
      0, 1, 3, // first triangle
      1, 2, 3 // second triangle
    },
    vbo,
    vao,
    ebo;

  glGenVertexArrays (1, &vao);
  glGenBuffers (1, &vbo);
  glGenBuffers (1, &ebo);

  glBindVertexArray(vao);
  glBindBuffer (GL_ARRAY_BUFFER, vbo);
  glBufferData (GL_ARRAY_BUFFER, sizeof (vertices), vertices, GL_STATIC_DRAW);

  glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, ebo);
  glBufferData (GL_ELEMENT_ARRAY_BUFFER, sizeof (indices), indices, GL_STATIC_DRAW);

  // Position attribute:
  glVertexAttribPointer (0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof (float), static_cast<void *> (0));
  glEnableVertexAttribArray (0);				     
  // Colour attribute:					     
  glVertexAttribPointer (1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof (float), reinterpret_cast<void *> (3 * sizeof (float)));
  glEnableVertexAttribArray (1);				     
  // Texture coord attribute:				     
  glVertexAttribPointer (2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof (float), reinterpret_cast<void *> (6 * sizeof (float)));
  glEnableVertexAttribArray (2);

  // Texture:
  unsigned int texture;
  glGenTextures (1, &texture);
  glBindTexture (GL_TEXTURE_2D, texture);
  glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
  glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

  int width, height, nr_channels;
  stbi_set_flip_vertically_on_load(true);
  unsigned char *data = stbi_load(TEXTURE_DIR "/gnu.png", &width, &height, &nr_channels, 0);
  if (!data)
    {
      std::cout << "Failed to load texture!\n";
    }
  glTexImage2D (GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
  glGenerateMipmap (GL_TEXTURE_2D);
  stbi_image_free (data);
  
  std::jthread input_thread ([&] ()
  {
    while (!glfwWindowShouldClose (window))
      {
	process_input ();
      }
  });

  glEnable(GL_DEPTH_TEST);
  glUseProgram (shader_program);
  glBindTexture (GL_TEXTURE_2D, texture);
  
  // Render loop:
  while (!glfwWindowShouldClose (window))
    { 
      glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

      glm::mat4 model = glm::mat4 (1.0f); // Make sure to initialize matrix to identity matrix first.
      glm::mat4 view = glm::mat4 (1.0f);
      glm::mat4 projection = glm::mat4 (1.0f);
      model = glm::scale (model, glm::vec3 (0.75, 1.0, 1.0));
      view  = glm::translate (view, glm::vec3 (0.0f, 0.0f, -1.0f));
      model = glm::rotate (model, static_cast<float> (glfwGetTime()) * glm::radians(50.0f), glm::vec3 (0.5f, 1.0f, 0.0f));
      projection = glm::perspective (glm::radians(105.0f), static_cast<float> (width) / static_cast<float> (height), 0.1f, 100.0f);
      unsigned int model_loc = glGetUniformLocation (shader_program, "model");
      unsigned int view_loc  = glGetUniformLocation (shader_program, "view");
      glUniformMatrix4fv (model_loc, 1, GL_FALSE, glm::value_ptr(model));
      glUniformMatrix4fv (view_loc, 1, GL_FALSE, &view[0][0]);
      glUniformMatrix4fv (glGetUniformLocation(shader_program, "projection"), 1, GL_FALSE, &projection[0][0]);
      glUniformMatrix4fv (glGetUniformLocation(shader_program, "view"), 1, GL_FALSE, &view[0][0]);
      
      glBindVertexArray (vao);
      glDrawElements (GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
      
      glfwSwapBuffers (window);
      glfwPollEvents ();
    }

  glfwTerminate ();
}
